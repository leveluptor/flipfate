function callAjax(url, data, method, callback) {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (req.readyState == XMLHttpRequest.DONE && req.status >= 200 && req.status < 400) {
            callback(req);
        }
    };
    req.open(method, url, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(data);
}

var getTopic = function() {
    var href = window.location.href;
    callAjax(href + '/topic', null, 'GET', function(resp) {
        document.getElementsByClassName('topic')[0].innerHTML = resp.responseText
    })

};

var createSortition = function () {
    var lot = {};
    lot['name'] = document.getElementById('name').value;
    lot['num'] = parseInt(document.getElementById('num').value);
    lot['marked'] = parseInt(document.getElementById('marked').value);

    console.log(lot);

    callAjax('app', JSON.stringify(lot), 'POST', function (resp) {
        var url = resp.getResponseHeader('Location');
        var link = document.getElementById('link');
        link.href = url;
        link.innerHTML = url;
        document.getElementsByClassName('link-container')[0].classList.remove('hidden');
    })

};

var CardStyles = Object.freeze({OTHERS: "", OWNER: "clickable"});

var createCard = function (cssClass, index) {

    var card = document.createElement("div");

    card.innerHTML = cardTemplate;

    card.id = "card" + index;

    card.classList.add('card');

    card.getElementsByClassName("front-name")[0].innerHTML = "";

    if (cssClass === CardStyles.OWNER) {
        card.classList.add(cssClass);
        card.getElementsByClassName("front-name")[0].innerHTML = "Flip me!";
        card.onclick = function () {
            var msg = {};
            msg['name'] = 'open_card';
            msg['params'] = {index: index};
            WS.send(JSON.stringify(msg));
        };
    }
    return card
};

var cardTemplate = '<div class="front">\
    <p class="front-name">Front placeholder</p>\
    </div>\
    <div class="back">\
    <p class="back-name">Name placeholder</p>\
    <p class="result">Result placeholder</p>\
    </div>';

var WS = function () {

    var socket;

    return {
        disconnect: function () {
            socket.close()
        },
        send: function (msg) {
            if (socket.readyState == WebSocket.OPEN) {
                socket.send(msg)
            }
        },
        connect: function () {
            var href = window.location.href;
            var roomId = href.substr(href.lastIndexOf('/') + 1);
            var name = document.getElementById('participantName').value;

            socket = new WebSocket("ws://" + window.location.host + "/flipfate/ws/" + roomId + "?name=" + name);

            socket.onmessage = function (event) {
                console.log(event.data);
                var msg = JSON.parse(event.data);

                if (msg['name'] === 'card_opened') {
                    console.log("Card opened event");
                    var index = msg['params']['index'];
                    var result = msg['params']['result'];
                    var name = msg['params']['name'];

                    var toTurn = document.getElementById('container').children[index];
                    toTurn.getElementsByClassName('back')[0].getElementsByClassName('result')[0].innerHTML = result;
                    toTurn.getElementsByClassName('back')[0].getElementsByClassName('back-name')[0].innerHTML = name;

                    document.querySelector("#card" + index).classList.toggle("flip");
                    document.querySelector("#card" + index).onclick = null;
                } else if (msg['name'] === 'create_cards') {
                    console.log("Create cards event");
                    var num = msg['params']['num'];
                    var pos = msg['params']['pos'];
                    for (var i = 0; i < num; i++) {
                        var card;
                        if (i === pos) {
                            card = createCard(CardStyles.OWNER, i);
                        } else {
                            card = createCard(CardStyles.OTHERS, i);
                        }
                        document.getElementById('container').appendChild(card);
                    }
                } else {
                    console.log("Other event");
                }


            };

            socket.onopen = function () {

            };

            document.getElementsByClassName('nameInput')[0].classList.add('collapsed')

        }
    };
}();