package com.leveluptor.flipfate.services

import java.util.UUID

public class UuidGenerator {

    public fun generate(): String {
        val id = UUID.randomUUID()
        return id.toString().replace("-".toRegex(), "")
    }
}