package com.leveluptor.flipfate.services

import java.util.ArrayList
import java.util.Collections
import java.util.Random
import java.util.TreeMap

public class SortitionEvaluator {

    fun generateOutcome(num: Int, marked: Int): Map<Int, Boolean> {

        val map = TreeMap<Int, Boolean>()

        for (i in 0..num - 1) {
            map.put(i, false)
        }

        if (marked == 1) {
            map.put(Random().nextInt(num), true)
        } else {

            val list = ArrayList(map.entrySet())

            Collections.shuffle(list)

            list.take(marked).forEach { item -> item.setValue(true) }
        }


        return map
    }


}

