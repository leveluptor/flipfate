package com.leveluptor.flipfate.services

import com.leveluptor.flipfate.controllers.SortitionConfig
import com.leveluptor.flipfate.endpoints.stripDangerousChars
import java.time.LocalDateTime
import java.util.concurrent.ConcurrentHashMap

public object SortitionRegistry {
    val map = ConcurrentHashMap<String, LotStatus>()
}

public class SortitionCreator {

    val idGenerator = UuidGenerator()

    fun create(sortition: SortitionConfig): String {

        var idCandidate: String

        do {
            idCandidate = idGenerator.generate()
        } while (SortitionRegistry.map.containsKey(idCandidate))

        val id = idCandidate

        val outcome = SortitionEvaluator().generateOutcome(sortition.num, sortition.marked)
        SortitionRegistry.map.put(id, LotStatus(stripDangerousChars(sortition.name), outcome))

        return id

    }

}

data class LotStatus(val topic: String, val outcome: Map<Int, Boolean>,
                     private var placesLeft: Int = outcome.size(),
                     val timestamp: LocalDateTime = LocalDateTime.now()) {

    fun tryToJoin() : Boolean {
        if (placesLeft > 0) {
            placesLeft--
            return true
        } else {
            return false
        }
    }

    fun isFull() : Boolean {
        return placesLeft == 0
    }
}
