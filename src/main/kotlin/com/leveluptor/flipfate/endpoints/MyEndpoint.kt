package com.leveluptor.flipfate.endpoints;

import com.fasterxml.jackson.core.SerializableString
import com.fasterxml.jackson.core.io.CharacterEscapes
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.leveluptor.flipfate.services.SortitionRegistry
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import java.util.ArrayList
import java.util.HashMap
import java.util.concurrent.ConcurrentHashMap
import javax.websocket.*
import javax.websocket.server.PathParam
import javax.websocket.server.ServerEndpoint

@ServerEndpoint(value = "/ws/{id}",
        decoders = arrayOf(CommandDecoder::class),
        encoders = arrayOf(CommandEncoder::class))

public class MyEndpoint {


    object SessionRegistry {
        val id2sessions: MutableMap<String, MutableList<Session>> = ConcurrentHashMap()
    }

    fun getQueryMap(query: String): Map<String, String> {
        val map: MutableMap<String, String> = HashMap()

        query.split("&".toRegex()).forEach { pair ->
            val keyvalue = pair.split("=".toRegex());
            map.put(keyvalue.get(0), keyvalue.get(1))
        }

        return map;
    }

    @OnOpen
    public fun onOpen(@PathParam("id") id: String, session: Session) {

        if (!SortitionRegistry.map.containsKey(id)) {
            throw IllegalArgumentException("No such id!!111")
        }

        val queryParams = getQueryMap(session.getQueryString())

        val unsafeName = URLDecoder.decode(queryParams.get("name")!!, "UTF-8")

        val saferName = stripDangerousChars(unsafeName)

        println("name: $unsafeName")

        session.getUserProperties().put("name", saferName)


        val status = SortitionRegistry.map.get(id)

        val sessionClients = SessionRegistry.id2sessions.getOrPut(id, { ArrayList<Session>() })


        sessionClients.add(session)

        var result = status.tryToJoin()

        if (!result) {
            return
        }

        println("size:${sessionClients.size()}")

        val num = SortitionRegistry.map.get(id).outcome.size()

        val pos = sessionClients.indexOf(session)

        val command = WebsocketCommand(name = "create_cards", params = mapOf("num" to num, "pos" to pos))

        session.getBasicRemote().sendObject(command)

        for (i in 0..SessionRegistry.id2sessions.get(id)!!.size() - 1) {
            if (true == SessionRegistry.id2sessions.get(id)!!.get(i).getUserProperties().get("opened")) {
                val outcome = SortitionRegistry.map.get(id).outcome.get(i)!!
                val outcomeString = if (outcome) "\u2718" else ""
                session.getBasicRemote().sendObject(buildCardTurnUpdate(i, outcomeString,
                        SessionRegistry.id2sessions.get(id)!!.get(i).getUserProperties().get("name").toString()))
            }
        }

        if (status.isFull()) {
            println(SessionRegistry.id2sessions.get(id)!!.size())
            SessionRegistry.id2sessions.get(id)!!.forEach { s -> s.getBasicRemote().sendText("{\"status\":\"full!\"}") }
        }

    }

    private fun buildCardTurnUpdate(index: Int, result: String, name: String): WebsocketCommand {
        return WebsocketCommand("card_opened", mapOf("index" to index, "result" to result, "name" to name))
    }

    @OnMessage
    public fun onMessage(@PathParam("id") id: String, message: WebsocketCommand, session: Session) {

        println(message)

        when (message.name) {
            "open_card" -> {
                val index = message.params.get("index") as Int
                val result = SortitionRegistry.map.get(id).outcome.get(index)
                val outcomeString = if (result!!) "\u2718" else ""

                session.getUserProperties().put("opened", true)

                SessionRegistry.id2sessions.get(id)!!.forEach { s ->
                    s.getBasicRemote().sendObject(buildCardTurnUpdate(index, outcomeString, session.getUserProperties().get("name").toString()))
                }
            }
            else -> {
                throw IllegalArgumentException("Nooo!!11: ${message.name}")
            }
        }
    }

    @OnClose
    public fun onClose(@PathParam("id") id: String, session: Session) {

    }

}

public class CommandDecoder : Decoder.Text<WebsocketCommand> {

    val reader = ObjectMapper().registerKotlinModule()

    override fun destroy() {

    }

    override fun init(config: EndpointConfig?) {

    }

    override fun willDecode(s: String?): Boolean {
        return s != null
    }

    override fun decode(s: String): WebsocketCommand? {
        return reader.readValue(s, javaClass<WebsocketCommand>())
    }
}

public class CommandEncoder : Encoder.Text<WebsocketCommand> {

    val writer: ObjectWriter = ObjectMapper().registerKotlinModule().writerWithDefaultPrettyPrinter()

    override fun init(config: EndpointConfig?) {
    }

    override fun destroy() {
    }

    override fun encode(obj: WebsocketCommand?): String? {
        return writer.writeValueAsString(obj)
    }
}

data class WebsocketCommand(val name: String, val params: Map<String, Any> = HashMap<String, Any>())

fun stripDangerousChars(unsafeString: String) = unsafeString.replace("[<>&'\"]".toRegex(), " ")