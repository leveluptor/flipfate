package com.leveluptor.flipfate.controllers;

import com.leveluptor.flipfate.endpoints.MyEndpoint
import com.leveluptor.flipfate.services.SortitionCreator
import com.leveluptor.flipfate.services.SortitionRegistry
import java.io.InputStream
import java.util.ArrayList
import java.util.concurrent.ConcurrentHashMap
import javax.servlet.ServletContext
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriBuilder

data class SortitionConfig(val name: String, val num: Int, val marked: Int)

@Path("/")
public class SortitionConfigController(@Context var context: ServletContext) {

    val sortitionCreator = SortitionCreator()

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public fun submitNewLot(sortition: SortitionConfig): Response {

        val id = sortitionCreator.create(sortition)

        val uri = UriBuilder.fromPath("$id").build()
        return Response.created(uri).build()
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.TEXT_HTML)
    public fun getSortitionPage(@PathParam("id") id: String): InputStream {
        return context.getResourceAsStream("/sortition.html")
    }

    @GET
    @Path("{id}/topic")
    @Produces(MediaType.TEXT_PLAIN)
    public fun getSortitionTopic(@PathParam("id") id: String): String {
        return SortitionRegistry.map.get(id).topic;
    }

}
