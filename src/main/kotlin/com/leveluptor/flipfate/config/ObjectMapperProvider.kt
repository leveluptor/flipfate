package com.leveluptor.flipfate.config

import com.fasterxml.jackson.core.SerializableString
import com.fasterxml.jackson.core.io.CharacterEscapes
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import javax.ws.rs.ext.ContextResolver
import javax.ws.rs.ext.Provider

@Provider
public class ObjectMapperProvider : ContextResolver<ObjectMapper> {

    companion object {
        private val kotlinAwareMapper = ObjectMapper().registerModule(KotlinModule())

        init {
            kotlinAwareMapper.getFactory().setCharacterEscapes(HtmlCharacterEscapes())
        }
    }

    override fun getContext(type: Class<*>): ObjectMapper {
        return kotlinAwareMapper
    }

}

public class HtmlCharacterEscapes : CharacterEscapes() {

    val escapes : IntArray;

    init {
        val esc = CharacterEscapes.standardAsciiEscapesForJSON();
        esc['<'.toInt()] = CharacterEscapes.ESCAPE_STANDARD;
        esc['>'.toInt()] = CharacterEscapes.ESCAPE_STANDARD;
        esc['&'.toInt()] = CharacterEscapes.ESCAPE_STANDARD;
        esc['\''.toInt()] = CharacterEscapes.ESCAPE_STANDARD;
        esc[' '.toInt()] = CharacterEscapes.ESCAPE_NONE;
        escapes = esc;
    }

    override fun getEscapeCodesForAscii(): IntArray {
        return escapes
    }

    override fun getEscapeSequence(ch: Int): SerializableString? {
        return null
    }
}
