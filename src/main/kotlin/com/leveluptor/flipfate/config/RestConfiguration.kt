package com.leveluptor.flipfate.config

import javax.ws.rs.ApplicationPath
import javax.ws.rs.core.Application

@ApplicationPath("app")
public class RestConfiguration : Application() {

}
